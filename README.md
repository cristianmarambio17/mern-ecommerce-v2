## MERN eCommerce 

![eCommerce](https://github.com/zeeshanhshaheen/client-ecommerce/blob/master/images/ee.jpeg)


![npm](https://img.shields.io/badge/npm-5.1.0-orange) 
![npm](https://img.shields.io/badge/reactjs-17.0-blue)
![npm](https://img.shields.io/badge/Node%2Cjs-14.15.1-green)
![npm](https://img.shields.io/badge/express.js-4.17.1-red)
![npm](https://img.shields.io/badge/MongoDB-4.4.3-brightgreen)


<table width='100%'>
    <tr>
        <td align='left' width='100%' colspan='2'>
            🎯  <code>eCommerce Site</code> is a MERN stack web application 
        </td>
    </tr>
    <tr>
        <td>
            Hers is the demo of <a href='https://merncommerceshop.herokuapp.com/'>eCommerce Site</a>.
        </td>
   
</table>


##  :white_flower: Features of `eCommerce Site` 

- Full featured shopping cart
- Product reviews and ratings
- Top products carousel
- Product pagination
- Product search feature
- User profile with orders
- Admin product management
- Admin user management
- Admin Order details page
- Mark orders as delivered option
- Checkout process (shipping, payment method, etc)
- PayPal / credit card integration
- Database seeder (products & users)


## Tech stack used
   - MongoDB
   - Express.js
   - Reactjs
   - Node.js
   - React Bootstrap v4
   - Redux



## 🔥 Getting Started?

#### Env Variables

Create a .env file in then root and add the following

```
NODE_ENV = development
PORT = 5000
MONGO_URI = your mongodb uri
JWT_SECRET = 'abc123'
PAYPAL_CLIENT_ID = your paypal client id
```

#### Install dependencies for frontend and backend

```
npm install
cd client
npm install
```


## Setup on local machine

- Download or clone the project
-  open your terminal 
- In the root folder run  ``` npm run dev ``` to run the project


## Instructions for user
- On the main page click on the `Register or Login` button to register or login
- You can see all the products listed
- Click on product to view and byt the product
- Click on the `cart` button to view your products that you have been added
- Add address and pay using PayPal


## Instructions for Admin
 - On the main page click on the `Register or Login` button to register or login
 - On the login page, enter the following credentials
    - `Email`: admin@gmail.com
    - `Password`: 123456
            
 - After you logged in you will see list of products, now you can edit and delete them
 - You can create product by clicking "Admin" button on navbar and products
 - You can view, edit, delete users
 - You can Mark as delievered orders



## CRISTIAN MARAMBIO - DOCKER & KUBERNETES - ESCALAB

<table width='100%'>
    <tr>
        <td align='left' width='100%' colspan='2'>
            <code>eCommerce Site</code> con extensiones para docker y kubernetes.
        </td>
    </tr>
    <tr>
        <td>
            La url del proyecto subido a GCP es : <a href='http://34.73.63.223:8080/'>eCommerce Site Dockerizado</a>.
        </td>
   
</table>

## 🔥 Instrucciones de como se levanto el proyecto

- En GCP se crea un proyecto que llame : <code> examencristianmarambio </code> y la ubicacion del cluster esta en us-east1-b
![Cluster](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/cluster.jpg)
-  Al estar en el cluster es necesario crear un namespace llamado <code> exa </code>
```
kubectl create namespace exa
```

- Posterior a eso es necesario crear un secret con variables de entorno que nos permitiran la conexion a la BD. Tenemos 2 opciones: MongoDBAtlas en un cluster aparte o MongoDB en el mismo cluster. Dejare los dos secretos en caso de que se cierre la de atlas ya que no es de pago. Pero se probo con ambas.

#### Local
```
kubectl create secret generic secret-examen --from-literal=JWT_SECRET="abc123" --from-literal=PAYPAL_CLIENT_ID=12342143 --from-literal=MONGO_URI="mongodb://mi-servicio-mongo-examen.exa.svc.cluster.local:27017" --from-literal=PORT="8080" --from-literal=NODE_ENV=development -n exa
```
#### Atlas
```
kubectl create secret generic secret-examen --from-literal=JWT_SECRET="abc123" --from-literal=PAYPAL_CLIENT_ID=12342143 --from-literal=MONGO_URI="mongodb+srv://root:root@clustermongodb.vvlt2.mongodb.net/test" --from-literal=PORT="8080" --from-literal=NODE_ENV=development -n exa
```

- Ahora lo siguiente es crear las imagenes correspondientes al back y al front con la siguiente nomenclatura: <code> nombreHost/nombreProyecto/nombreImagen </code>

#### Back
```
En la raiz del proyecto
docker build -t us.gcr.io/examencristianmarambio/imagen-back-examen .
```
#### Front
```
cd client
docker build -t us.gcr.io/examencristianmarambio/imagen-front-examen .
```

- Al crear las imagenes es necesario enviarlas a la nube
```
docker push us.gcr.io/examencristianmarambio/imagen-front-examen
docker push us.gcr.io/examencristianmarambio/imagen-back-examen
```
![ImagenCluster](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/imagenCluster.jpg)

- Para finalizar ejecutar los deployment correspondientes a cada pod: front, back, MongoDb

#### Mongo deployment Local
```
En caso de usar la BD en el mismo cluster
En la raiz del proyecto
kubectl apply -f deployment-mongo.yaml
```
#### Back deployment
```
cd backend
kubectl apply -f deployment.yaml
```
#### Front deployment
```
cd client
kubectl apply -f deployment.yaml
```

#### Deployments
![ImagenDeployments](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/deployments.jpg)

#### Services
![ImagenServices](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/services.jpg)

#### Pods
![ImagenPods](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/pods.jpg)

- Para crear los datos de inicio es necesario entrar al pod del backend:
```
kubectl exec -it (nombre Pod) -n exa sh
```
y ejecutar el siguiente comando
```
npm run data:import
```


## 🔥 ANEXO

- Se adjunta json serviceAccount en la carpeta ServiceAccount con permisos de cluster e imagenes llamado 
```
examencristianmarambio-349476ab5612.json
```
#### Proyecto Levantado
![ProyectoLevantado](https://github.com/titoncio69/mern-ecommerce-v2/blob/master/imagenesGCP/proyectoLevantado.jpg)
