import bcrypt from "bcryptjs";

const users = [
  {
    name: "cristian",
    email: "cristianmarambio17@gmail.com",
    password: bcrypt.hashSync("123", 10),
    isAdmin: true,
  },
  {
    name: "zeeshan",
    email: "zeeshan@gmail.com",
    password: bcrypt.hashSync("123456", 10),
  },
  {
    name: "shan",
    email: "shan@gmail.com",
    password: bcrypt.hashSync("123456", 10),
  },
];

export default users;
